## Reforzamiento de JavaScript y TypeScript

## Ejecutar e instalar

1. Instalar dependencias ```npm install```
2. Ejecutar ```npm run dev``` para iniciar el servidor de desarrollo
3. Cambiar el main.ts, acorde a lo que se quiera probar
4. Recuerda generar el [ApiKey de Giphy](https://developers.giphy.com/dashboard/) si quieres probar esa parte.

# Temas puntuales de la sección

Esta es una sección de reforzamiento en JavaScript y TypeScript, la cual es necesaria para no sentir la curva de aprendizaje de Vue.js complicada.

## Puntualmente, quiero reforzar los siguientes temas:

- Constantes, let y var
- Template literals
- Objetos
- Arreglos
- Funciones
- Funciones de flecha
- Desestructuración de objetos y arreglos
- Importaciones y exportaciones
- Promesas
- Argumentos a promesas
- Valor y referencia
- Async y Await
- Peticiones HTTP
- Ternarios
- Null check
- TypeScript

En general, trabajaremos haciendo ejercicios, que son altamente recomendados si no estás seguro si sabes lo suficiente de JavaScript.
