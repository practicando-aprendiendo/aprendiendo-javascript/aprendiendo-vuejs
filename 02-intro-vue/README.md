# Ejercicio introductorio de VueJS con CDN


Pueden clonar o descargar el proyecto y ejecutar el index.html en un servidor local.

# Temas puntuales de la sección

En esta sección comenzaremos al fin nuestro camino en VueJS, puntualmente veremos sobre:

- CompositionAPI vs OptionsAPI
- Hola Mundo en Vue
- Vue mediante CDN
- Propiedades reactivas con Ref
- Propiedades de solo lectura con computed
- Función para crear una aplicación de Vue (`createApp`)
- Eventos y modificadores de evento
  - Click
  - Keypress
  - Enter
  - Invocar funciones
- Directivas como
  - `v-if`
  - `v-for`
  - `v-show`
