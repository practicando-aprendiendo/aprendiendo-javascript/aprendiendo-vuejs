# Temas puntuales de la sección

En esta sección empezaremos a dejar las bases de cómo estaremos trabajando en nuestras aplicaciones de Vue.js de aquí en adelante, pero puntualmente veremos sobre:

-   Composition API
-   Script Setup
-   `DefineComponent`
-   Single File Components
-   Separación de Lógica
-   Composables
-   Build de producción
-   Despliegue a producción
-   Manejo de estilos
    -   Globales
    -   Bootstrap
    -   Tailwind

# Temas puntuales de la sección

En esta sección comenzaremos a crear una aplicación real en Vue, que nos sirva para aprender sobre:

-   Escuchar y emitir eventos
-   Enviar propiedades
-   Trabajar con la reactividad de Vue.js
-   Composables
-   Tailwind
-   Referencias a elementos HTML
-   Peticiones HTTP
-   Separación de componentes

# Temas puntuales de la sección

Esta es nuestra primera sección sobre el testing, y aquí veremos sobre:

-   Configuraciones manuales
-   TypeScript en pruebas automáticas
-   Vitest
-   Expect
-   Spies
-   `Vi.fn`
-   Wrappers
-   Mounts
-   Virtual View Model
-   `Expect.any`
-   Peticiones HTTP en testing
-   Fake de APIs nativas como el `fetch`
-   Cobertura - Coverage

# 03-indecision-app

This template should help get you started developing with Vue 3 in Vite.

## Recommended IDE Setup

[VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (and disable Vetur).

## Type Support for `.vue` Imports in TS

TypeScript cannot handle type information for `.vue` imports by default, so we replace the `tsc` CLI with `vue-tsc` for type checking. In editors, we need [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) to make the TypeScript language service aware of `.vue` types.

## Customize configuration

See [Vite Configuration Reference](https://vitejs.dev/config/).

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Type-Check, Compile and Minify for Production

```sh
npm run build
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```
