import { mount } from '@vue/test-utils'
import MyCounter from '@/components/MyCounter.vue'

describe('<MyCounter />', () => {
    test('should match snapshot', () => {
        const wrapper = mount(MyCounter, {
            props: {
                value: 5,
            },
        })

        expect(wrapper.html()).toMatchSnapshot()
    })

    test('renders the counter value correctly', () => {
        const initialValue = 5

        const wrapper = mount(MyCounter, {
            props: {
                value: initialValue,
            },
        })

        const [counterLabel, squareLabel] = wrapper.findAll('h3')

        expect(wrapper.find('h3').text()).contain(`Counter: ${initialValue}`)
        expect(wrapper.find('[data-testid="square-label"]').text()).contain(
            `Square: ${initialValue * initialValue}`,
        )

        expect(counterLabel.text()).toContain(`Counter: ${initialValue}`)
        expect(squareLabel.text()).toContain(`Square: ${initialValue * initialValue}`)
    })

    test('increments de counter when +1 button is clicked', async () => {
        const initialValue = 5

        const wrapper = mount(MyCounter, {
            props: {
                value: initialValue,
            },
        })

        const [counterLabel, squareLabel] = wrapper.findAll('h3')

        const btnIncrement = wrapper.find('button')
        await btnIncrement.trigger('click')

        expect(counterLabel.text()).toContain(`Counter: ${initialValue + 1}`)
        expect(squareLabel.text()).toContain(`Square: ${(initialValue + 1) * (initialValue + 1)}`)
    })

    test('decrements de counter when -1 button is clicked twice', async () => {
        const initialValue = 5

        const wrapper = mount(MyCounter, {
            props: {
                value: initialValue,
            },
        })

        const [counterLabel, squareLabel] = wrapper.findAll('h3')

        const btnDecrement = wrapper.find('[data-decrement-btn="decrement-btn"]')

        await btnDecrement.trigger('click')
        await btnDecrement.trigger('click')

        expect(counterLabel.text()).toContain(`Counter: ${initialValue - 2}`)
        expect(squareLabel.text()).toContain(`Square: ${(initialValue - 2) * (initialValue - 2)}`)
    })
})
