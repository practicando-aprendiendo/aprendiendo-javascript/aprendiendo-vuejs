import ChatBubble from '@/components/Chat/ChatBubble.vue'
import { mount } from '@vue/test-utils'

describe('<ChatBubble />', () => {
    test('renders own message correctly', () => {
        const message = 'Hola mundo'
        const wrapper = mount(ChatBubble, {
            props: {
                message,
                itsMine: true,
            },
        })

        // const element = wrapper.find('.bg-blue-200').exists()
        // expect(element).toBe(true)

        expect(wrapper.find('.bg-blue-200').exists()).toBe(true)
        expect(wrapper.find('.bg-blue-200').exists()).toBeTruthy()
        expect(wrapper.find('.bg-blue-200').text()).contain(message)
        expect(wrapper.find('.bg-gray-300').exists()).toBeFalsy()
    })

    test('renders received message correctly', () => {
        const message = 'No!!'
        const wrapper = mount(ChatBubble, {
            props: {
                message,
                itsMine: false,
            },
        })

        expect(wrapper.find('.bg-gray-300').exists()).toBe(true)
        expect(wrapper.find('.bg-gray-300').exists()).toBeTruthy()
        expect(wrapper.find('span').text()).contain(message)
        expect(wrapper.find('.bg-blue-200').exists()).toBeFalsy()
        expect(wrapper.find('img').exists()).toBeFalsy()
    })

    test('renders received message correctly an image', () => {
        const message = 'No!!'
        const image = 'test.png'

        const wrapper = mount(ChatBubble, {
            props: {
                message,
                itsMine: false,
                image,
            },
        })

        expect(wrapper.find('.bg-gray-300').exists()).toBe(true)
        expect(wrapper.find('.bg-gray-300').exists()).toBeTruthy()
        expect(wrapper.find('span').text()).contain(message)
        expect(wrapper.find('.bg-blue-200').exists()).toBeFalsy()
        expect(wrapper.find('img').exists()).toBeTruthy()
        expect(wrapper.find('img').attributes('src')).toBe(image)
    })
})
