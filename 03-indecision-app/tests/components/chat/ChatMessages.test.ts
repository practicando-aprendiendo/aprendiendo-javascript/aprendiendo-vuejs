import ChatMessages from '@/components/Chat/ChatMessages.vue'
import type { ChatMessage } from '@/interfaces/chat-message.interface'
import { mount } from '@vue/test-utils'

const messages: ChatMessage[] = [
    {
        id: 1,
        message: 'Hola Mundo',
        itsMine: true,
    },
    {
        id: 2,
        message: 'No!!',
        itsMine: false,
        image: 'http://test.png',
    },
]

describe('<ChatMessages/>', () => {
    const wrapper = mount(ChatMessages, {
        props: {
            messages,
        },
    })

    test('render chat messages correctly', () => {
        const chatBubble = wrapper.findAllComponents({
            name: 'ChatBubble',
        })

        expect(chatBubble.length).toBe(messages.length)
    })

    test('scrolls down to the bottom after messages update', async () => {
        const scrollToMock = vi.fn()

        console.log(wrapper.vm.$refs.chatRef)

        const chatRef = wrapper.vm.$refs.chatRef as HTMLDivElement
        chatRef.scrollTo = scrollToMock

        await wrapper.setProps({
            messages: [...messages, { id: 3, message: 'Hey', itsMine: true }],
        })

        await new Promise((r) => setTimeout(r, 150))

        expect(scrollToMock).toHaveBeenCalledTimes(1)
        expect(scrollToMock).toHaveBeenCalledWith({
            behavior: 'smooth',
            top: expect.any(Number),
        })
    })
})
