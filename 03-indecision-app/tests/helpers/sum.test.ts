// sum.test.js
import { describe, expect, test } from 'vitest'
import { sum, addArray } from '../../src/helpers/sum'

describe('add function', () => {
    test('adds 1 + 2 to equal 3', () => {
        // preparacion
        const a = 1
        const b = 2

        // estimulo
        const result = sum(a, b)

        // El comportamiento esperado
        expect(result).toBe(a + b)

        // if (sum(1, 2) !== 4) {
        //     throw new Error('La suma no es correcta')
        // }
    })
})

describe('addArray function', () => {
    test('debe retornar 0 si el array esta vacio', () => {
        // preparacion
        const array = []

        // estimulo
        const result = addArray(array)

        // El comportamiento esperado
        expect(result).toBe(0)
    })

    test('Sumatoria de elementos del array', () => {
        // preparacion
        const array = [1, 2, 3]

        // estimulo
        const result = addArray(array)

        // El comportamiento esperado
        expect(result).toBe(6)
    })
})
