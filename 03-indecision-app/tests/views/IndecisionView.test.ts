import ChatMessages from '@/components/Chat/ChatMessages.vue'
import MessageBox from '@/components/Chat/MessageBox.vue'
import IndecisionView from '@/views/IndecisionView.vue'
import { mount } from '@vue/test-utils'

const mockChatMessages = {
    template: `
    <div data-testid="mock-messages">
        Mock ChatMessages
    </div>
    `,
}

describe('<IndecisionView />', () => {
    test('renders chat messages and messagebox correctly', () => {
        const wrapper = mount(IndecisionView)

        expect(wrapper.findComponent(ChatMessages).exists()).toBeTruthy()
        expect(wrapper.findComponent(MessageBox).exists()).toBeTruthy()
    })

    test('calls onMessage when sendign a message', async () => {
        const wrapper = mount(IndecisionView, {
            global: {
                stubs: {
                    ChatMessages: mockChatMessages,
                },
            },
        })

        const messageBoxComponent = wrapper.findComponent(MessageBox)
        messageBoxComponent.vm.$emit('sendMessage', 'Hola mundo')

        await new Promise((r) => setTimeout(r, 150))
    })
})
