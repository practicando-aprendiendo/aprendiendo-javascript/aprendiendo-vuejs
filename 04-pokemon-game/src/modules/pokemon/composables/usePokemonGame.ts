import { computed, onMounted, ref } from 'vue'
import { GameStatus, type Pokemon, type PokemonListResponse } from '../interfaces'
import { pokemonApi } from '../api/pokemonApi'
import confetti from 'canvas-confetti'

export const usePokemonGame = () => {
    const gameStatus = ref<GameStatus>(GameStatus.Playing)
    const pokemonsOptions = ref<Pokemon[]>([])
    const pokemons = ref<Pokemon[]>([])

    const randomPokemon = computed(() => {
        const randomIndex = Math.floor(Math.random() * pokemonsOptions.value.length)
        return pokemonsOptions.value[randomIndex]
    }) // un pokemos de pokemonOptions
    const isLoading = computed(() => pokemons.value.length === 0)

    const getPokemons = async (): Promise<Pokemon[]> => {
        const response = await pokemonApi.get<PokemonListResponse>('/?limit=151')

        const pokemonsArray = response.data.results.map((pokemon) => {
            const urlParts = pokemon.url.split('/')
            const id = urlParts.at(-2) ?? 0
            return {
                name: pokemon.name,
                id: Number(id),
            }
        })

        return pokemonsArray.sort(() => Math.random() - 0.5)
        // console.log(response.data)
    }

    const getNextRound = (hoyMany: number = 4) => {
        gameStatus.value = GameStatus.Playing
        pokemonsOptions.value = pokemons.value.slice(0, hoyMany)
        pokemons.value = pokemons.value.slice(hoyMany)
    }

    const checkAnswer = (id: number) => {
        const hasWon = randomPokemon.value.id === id

        if (hasWon) {
            gameStatus.value = GameStatus.Won

            fireWorks()
            return
        }

        gameStatus.value = GameStatus.Lose
    }

    const fireWorks = () => {
        const duration = 3 * 1000
        const animationEnd = Date.now() + duration
        const defaults = { startVelocity: 30, spread: 360, ticks: 60, zIndex: 0 }

        const randomInRange = (min, max) => {
            return Math.random() * (max - min) + min
        }

        const interval = setInterval(function () {
            const timeLeft = animationEnd - Date.now()

            if (timeLeft <= 0) {
                return clearInterval(interval)
            }

            const particleCount = 50 * (timeLeft / duration)
            // since particles fall down, start a bit higher than random
            confetti({
                ...defaults,
                particleCount,
                origin: { x: randomInRange(0.1, 0.3), y: Math.random() - 0.2 },
            })
            confetti({
                ...defaults,
                particleCount,
                origin: { x: randomInRange(0.7, 0.9), y: Math.random() - 0.2 },
            })
        }, 250)
    }

    onMounted(async () => {
        pokemons.value = await getPokemons()
        getNextRound()
    })

    return {
        gameStatus,
        isLoading,
        pokemonsOptions,
        randomPokemon,

        // Methods
        checkAnswer,
        getNextRound,
    }
}
