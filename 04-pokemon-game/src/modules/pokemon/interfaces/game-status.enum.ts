export enum GameStatus {
    Playing = 'playing',
    Won = 'won',
    Lose = 'lose',
}
