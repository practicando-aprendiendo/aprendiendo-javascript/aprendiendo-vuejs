# Temas puntuales de la sección

En esta sección hablaremos extensamente sobre el Router de Vue, puntualmente veremos:

-   Rutas
-   Rutas Hijas y Padres
-   Navegación vía `RouterLink`
-   Estilo de la ruta activa
-   Ciclo de vida de los componentes
-   `KeepAlive`
-   Múltiples diseños estructurales
-   Layouts
-   `useRouter` y composables
-   Argumentos por URL
-   Protección de Rutas
-   Procesar y validar segmentos de ruta
-   Mucho más

Es una sección enfocada plenamente en el uso del Router y sus consideraciones.

# 05-vue-spa

This template should help get you started developing with Vue 3 in Vite.

## Recommended IDE Setup

[VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (and disable Vetur).

## Type Support for `.vue` Imports in TS

TypeScript cannot handle type information for `.vue` imports by default, so we replace the `tsc` CLI with `vue-tsc` for type checking. In editors, we need [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) to make the TypeScript language service aware of `.vue` types.

## Customize configuration

See [Vite Configuration Reference](https://vitejs.dev/config/).

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Type-Check, Compile and Minify for Production

```sh
npm run build
```

### Run Unit Tests with [Vitest](https://vitest.dev/)

```sh
npm run test:unit
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```
