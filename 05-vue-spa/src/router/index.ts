import isAuthenticatedGuard from '@/modules/auth/guards/is-authenticated'
import NotFound from '@/modules/common/pages/NotFound.vue'
import { createRouter, createWebHashHistory } from 'vue-router'

const router = createRouter({
    history: createWebHashHistory(import.meta.env.BASE_URL),
    routes: [
        {
            path: '/',
            name: 'landing',
            redirect: { name: 'home' },
            component: () => import('@/modules/landing/layouts/LandingLayout.vue'),
            children: [
                {
                    path: '/',
                    name: 'home',
                    component: () => import('@/modules/landing/pages/HomePage.vue'),
                },
                {
                    path: '/features',
                    name: 'features',
                    component: () => import('@/modules/landing/pages/FeaturesPage.vue'),
                },
                {
                    path: '/pricing',
                    name: 'pricing',
                    component: () => import('@/modules/landing/pages/PricingPage.vue'),
                },
                {
                    path: '/contact',
                    name: 'contact',
                    component: () => import('@/modules/landing/pages/ContactPage.vue'),
                },
                {
                    path: '/pokemons',
                    children: [
                        {
                            path: ':id',
                            name: 'pokemon',
                            // beforeEnter: [
                            //     (to, from, next) => {

                            //         return next()
                            //     }
                            // ]
                            beforeEnter: [isAuthenticatedGuard],
                            // props: true,
                            props: (route) => {
                                const id = Number(route.params.id)

                                return isNaN(id) ? { id: 1 } : { id }
                            },
                            component: () => import('@/modules/pokemons/pages/PokemonPage.vue'),
                        },
                    ],
                },
            ],
        },

        // Auth
        {
            path: '/auth',
            redirect: { name: 'login' },
            component: () => import('@/modules/auth/layouts/AuthLayout.vue'),
            children: [
                {
                    path: '/login',
                    name: 'login',
                    component: () => import('@/modules/auth/pages/LoginPage.vue'),
                },
                {
                    path: '/register',
                    name: 'register',
                    component: () => import('@/modules/auth/pages/RegisterPage.vue'),
                },
            ],
        },

        {
            path: '/:pathMatch(.*)*',
            name: 'NotFound',
            component: NotFound,
        },
    ],
})

export default router
