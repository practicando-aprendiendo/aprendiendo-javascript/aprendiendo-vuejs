# Temas puntuales de la sección

Esta sección principalmente toca el tema de los SLOTS de Vue, los cuales son una forma de enviar componentes desde el padre al hijo sin pasar por las props.

Pero puntualmente veremos:

-   Slots
-   Name Slots
-   Uso de slots y Templates
-   Tailwind
-   DaisyUI
-   Modals

# Temas puntuales de la sección

En esta sección comenzaremos a trabajar con Pinia como gestor de estado, el cual es muy poderoso y apoyado por la comunidad oficial de Vue con miembros de desarrollo de Vue Router, lo que lo hace una de las mejores opciones para controlar estado en aplicaciones de VueJS.

Puntualmente veremos:

-   Pinia
-   Setup Store
-   Getters
-   Actions
-   State
-   Consumo
-   Reactividad
-   `storeToRefs`

# 06-projects-app

This template should help get you started developing with Vue 3 in Vite.

## Recommended IDE Setup

[VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (and disable Vetur).

## Type Support for `.vue` Imports in TS

TypeScript cannot handle type information for `.vue` imports by default, so we replace the `tsc` CLI with `vue-tsc` for type checking. In editors, we need [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) to make the TypeScript language service aware of `.vue` types.

## Customize configuration

See [Vite Configuration Reference](https://vitejs.dev/config/).

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Type-Check, Compile and Minify for Production

```sh
npm run build
```

### Run Unit Tests with [Vitest](https://vitest.dev/)

```sh
npm run test:unit
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```
