import { defineStore } from 'pinia'
import { computed, ref } from 'vue'
import type { Project } from '../interfaces/project.interface'
import { v4 as uuidv4 } from 'uuid'
import { useLocalStorage } from '@vueuse/core'

export const useProjectsStore = defineStore('projects', () => {
    const projects = ref(useLocalStorage<Project[]>('projects', []))

    const addProject = (name: string) => {
        if (name.length === 0) return

        projects.value.push({
            id: uuidv4(),
            name: name,
            tasks: [],
        })
    }

    const addTaskToProject = (project: Project, taskName: string) => {
        if (taskName.length === 0) return

        project.tasks.push({
            id: uuidv4(),
            name: taskName,
        })
    }

    const toggleTask = (project: Project, taskId: string) => {
        const task = project.tasks.find((task) => task.id === taskId)

        if (!task) return

        task.completedAt = task.completedAt ? null : new Date()
    }

    return {
        // State
        projects,

        // Getters
        projectList: computed(() => [...projects.value]),
        emptyProjects: computed(() => projects.value.length === 0),
        projectsWithComplition: computed(() => {
            return projects.value.map((project) => {
                const total = project.tasks.length
                const completedTask = project.tasks.filter((task) => task.completedAt).length
                const complition = total === 0 ? 0 : (completedTask / total) * 100

                return {
                    id: project.id,
                    name: project.name,
                    taskCount: total,
                    complition: complition,
                }
            })
        }),

        // Actions
        addProject,
        addTaskToProject,
        toggleTask,
    }
})
