# Bienvenidos al curso de Vue.js de cero a experto!

Vue.js es un framework de JavaScript que durante los últimos años está creciendo de manera exponencial por su facilidad de integración, versatilidad y performance.

Este curso tiene por objetivo enseñarte Vue.js desde cero, pero si ya conoces el framework, también te será útil ya que rápidamente profundizaremos en conceptos fundamentales como las pruebas automáticas, Pinia y el nuevo Composition API + Script Setup.

## Aquí les dejo una lista de algunos puntos que tocaremos en el curso:

- Vue aislado progresivo
- Directivas
- Peticiones HTTP
- Pruebas unitarias y de integración
- Pinia
  - Actions
  - State
  - Getters
    - Getters simples
    - Getters como funciones
  - Modules
- Axios y Fetch API
  - Interceptores
- Autenticación
- Router
- Diferentes estructuras (Layouts)
- LazyLoad
- OptionsAPI (es mencionado para comprender que existe esta sintaxis)
- CompositionAPI
- Mocks
- Spies
- Comunicación entre componentes de diferentes formas
- Eventos
  - Modificadores de eventos
- Despliegues a producción
- Diferenciación entre Development, Test y Production
- Guards globales
- Guards por ruta
- Guards asíncronos y síncronos
- Carga de archivos
- Validación de imágenes
- Reutilización de componentes
- Composable Functions
- Tailwind
- Formularios
- Mucho más

Este curso de Vue.js cuenta con contenido exclusivo de Vue.js y temas relacionados directamente, el cual está pensado para que al terminar, puedas llevar a cabo las aplicaciones que tienes en mente sin ningún problema y realizar las pruebas automáticas de las mismas. Adicionalmente, se muestra y trabaja en proyectos pequeños, medianos y de gran escala, con el objetivo de darte ideas para el manejo de la estructura de directorios en un proyecto de gran tamaño.

## Lo que aprenderás

- Aprender Vue 3 a profundidad
- Vue Router
- Pinia
- Test unitarios
- Composition API
- Modularización y reutilización de código
- TypeScript en Vue
- Vitest
- Vite
- Script Setup

## ¿Hay requisitos para realizar el curso?

- Conocimiento de programación básica
- Conocimiento de JavaScript básico
- Poder realizar instalaciones en el equipo
- El curso se puede seguir en Windows, Linux o Mac OS X
- Conocimiento de TypeScript es opcional pero recomendado

## ¿Para quién es este curso?

- Personas sin ningún conocimiento de Vue
- Personas que quieran aprender lo nuevo sobre Vue 3
- Todos aquellos que quieran comprender cómo realizar pruebas automáticas
- Todos los que quieran mejorar sus habilidades en este framework
